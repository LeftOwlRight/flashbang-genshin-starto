
local ThisModPath = ModPath

--[[File Path]]
local ThisOGG = ThisModPath .. "sounds/cantaloupe.ogg"
local ThisTexture = "cantaloupe"

if blt.xaudio then
    blt.xaudio.setup()
else
    return
end

if not io.file_is_readable(ThisOGG) then
    return
end

local ThisModIds = Idstring(ThisModPath):key()
local __Name = function(__id)
    return "BADMEMES_" .. Idstring(tostring(__id) .. "::" .. ThisModIds):key()
end

local XAudioBuffer = __Name("XAudioBuffer")
local XAudioSource = __Name("XAudioSource")
local _GName = __Name("_G")
_G[_GName] = _G[_GName] or {}
local ThisBitmap = __Name("ThisBitmap")
_G[ThisBitmap] = _G[ThisBitmap] or nil

local function is_FlashBMemes()
    return
        type(FlashBMemes) == "table" and type(FlashBMemes.Options) == "table" and type(FlashBMemes.Options.GetValue) ==
            "function"
end

local function __ply_ogg()
    local __is_FlashBMemes = is_FlashBMemes()
    local __audio_chosen = __is_FlashBMemes and FlashBMemes.Options:GetValue("FGS_MultiSounds") or 1
    if __audio_chosen == 1 then
        return
    elseif __audio_chosen == 3 then
        ThisOGG = ThisModPath .. "sounds/cantaloupe_2.ogg"
    end

    if io.file_is_readable(ThisOGG) then
        local __volume_start = __is_FlashBMemes and FlashBMemes.Options:GetValue("__volume_start") or 0.5
        local this_buffer = XAudio.Buffer:new(ThisOGG)
        local this_source = XAudio.UnitSource:new(XAudio.PLAYER)
        this_source:set_buffer(this_buffer)
        this_source:play()
        this_source:set_volume(__volume_start)
        _G[_GName][XAudioBuffer] = this_buffer
        _G[_GName][XAudioSource] = this_source
    end

    local YSQD_message = {
        key_1 = ThisOGG,
        value_1 = "YSQD_play"
    }
    --LuaNetworking:SendToPeers('YSQD_play', json.encode(YSQD_message))

    return YSQD_message
end

local function __end_ogg()
    if _G[_GName][XAudioSource] then
        _G[_GName][XAudioSource]:close(true)
        _G[_GName][XAudioSource] = nil
    end
    if _G[_GName][XAudioBuffer] then
        _G[_GName][XAudioBuffer]:close(true)
        _G[_GName][XAudioBuffer] = nil
    end
    return
end

--[[local FSG_play_multiplier = FlashBMemes.Options:GetValue("FGS__multiplier") or 0.5
local FSG_in_multiplier = FlashBMemes.Options:GetValue("FGS_fade_in_multiplier") or 0.25
local FSG_out_multiplier = FlashBMemes.Options:GetValue("FGS_fade_out_multiplier") or 0.25
local FSG_multiplier_enable = FlashBMemes.Options:GetValue("FGS_try_real_duration") or true]]

local function __ply_pic(duration, fade_in_duration, fade_out_duration, current_flash, unchanged_duration)
    if current_flash == 999 then
        if managers and managers.mission and managers.mission._fading_debug_output then
            managers.mission._fading_debug_output:script().log("Flashbang time has something wrong, report to the author", Color.white)
            current_flash = 1
            managers.mission._fading_debug_output:script().log("Time is reset to 1", Color.white)
        end
    end

    local __is_FlashBMemes = is_FlashBMemes()
    local duration_Freeze = __is_FlashBMemes and FlashBMemes.Options:GetValue("FGS__duration") or duration
    local duration_Fade_in = __is_FlashBMemes and FlashBMemes.Options:GetValue("FGS_fade_in_duration") or fade_in_duration
    local duration_Fade_out = __is_FlashBMemes and FlashBMemes.Options:GetValue("FGS_fade_out_duration") or fade_out_duration
    
    if duration_Freeze == 999 or duration_Fade_in == 999 or duration_Fade_out == 999 then
        if duration_Freeze == 999 then
            managers.mission._fading_debug_output:script().log("duration_Freeze", Color.white)
        end
        if duration_Fade_in == 999 then
            managers.mission._fading_debug_output:script().log("duration_Fade_in", Color.white)
        end
        if duration_Fade_out == 999 then
            managers.mission._fading_debug_output:script().log("duration_Fade_out", Color.white)
        end
        if managers and managers.mission and managers.mission._fading_debug_output then
            managers.mission._fading_debug_output:script().log("Flashbang duration settings has something wrong, report to the author", Color.white)
            managers.mission._fading_debug_output:script().log("Value is reset", Color.white)
            duration_Freeze = 0.5
            duration_Fade_in = 0.25
            duration_Fade_out = 0.25
        end
    end

    local enable_fm = FlashBMemes.Options:GetValue("FGS_fully_multi_duration") or false
    local fully_multi = 1
    local fully_check = unchanged_duration - current_flash
    if fully_check < 0.35 and enable_fm then
        fully_multi = unchanged_duration - fully_check
    end
    
    if _G[ThisBitmap] then
        _G[ThisBitmap]:set_visible(true)
        _G[ThisBitmap]:animate(function(o)
            over(current_flash * duration_Fade_in * fully_multi, function(t)
                o:set_alpha(t)
            end)
            wait(current_flash * duration_Freeze * fully_multi)
            over(current_flash * duration_Fade_out * fully_multi, function(t)
                o:set_alpha(1 - t)
            end)
        end)

    return (current_flash * fully_multi  * (duration_Fade_in + duration_Freeze + duration_Fade_out))
    
    end
end

local function __end_pic()
    if _G[ThisBitmap] then
        _G[ThisBitmap]:set_visible(false)
    end
    return
end

if CoreEnvironmentControllerManager then
    Hooks:PostHook(CoreEnvironmentControllerManager, "set_flashbang", __Name("set_flashbang"), function(self)
        __end_ogg()
        local YSQD_message = __ply_ogg()
        --managers.mission._fading_debug_output:script().log("type "..type(YSQD_message), Color.white)
        -- managers.environment_controller:set_flashbang_multiplier()
        if not self.FSG_flashbang_du_log or self.FSG_flashbang_du_log ~= self._current_flashbang then
            self.FSG_flashbang_du_log = self._current_flashbang
        end
        --managers.mission._fading_debug_output:script().log("Flash Duration is "..self._current_flashbang.." and type is "..type(self._current_flashbang), Color.white)
        local last_time = __ply_pic(999, 999, 999, self.FSG_flashbang_du_log or self._current_flashbang or 999, self._flashbang_duration or 999) -- (FSG_flashbang_du_log * FSG_play_multiplier, FSG_flashbang_du_log * FSG_in_multiplier, FSG_flashbang_du_log * FSG_out_multiplier)
    
        if self._current_flashbang > 1 then
            --managers.mission._fading_debug_output:script().log("lua send", Color.white)
            LuaNetworking:SendToPeers('YSQD_play', json.encode(YSQD_message))
        end

    end)
end

--[[if CoreEnvironmentControllerManager then
	Hooks:PostHook(CoreEnvironmentControllerManager, "set_flashbang_multiplier", __Name("set_flashbang_multiplier"), function(self, multiplier)
		__end_ogg()
		__ply_ogg()
		local FSG_flashbang_du_log = self._flashbang_multiplier-- self._flashbang_duration

		managers.mission._fading_debug_output:script().log("Multiplier is "..multiplier, Color.white)
		managers.mission._fading_debug_output:script().log("Flash Duration is "..FSG_flashbang_du_log, Color.white)
		__ply_pic(0.5,0.25,0.25)--(FSG_flashbang_du_log * FSG_play_multiplier, FSG_flashbang_du_log * FSG_in_multiplier, FSG_flashbang_du_log * FSG_out_multiplier)
	end)
end]]

if PlayerDamage then
    Hooks:PostHook(PlayerDamage, "update", __Name("update"), function(self)
        if _G[_GName][XAudioSource] and not _G[_GName][XAudioSource]:is_active() then
            __end_ogg()
            -- __end_pic()
        end
    end)
    Hooks:PostHook(PlayerDamage, "_stop_tinnitus", __Name("_stop_tinnitus"), function(self)
        __end_ogg()
        __end_pic()
    end)
    Hooks:PreHook(PlayerDamage, "pre_destroy", __Name("pre_destroy"), function(self)
        __end_ogg()
        __end_pic()
    end)
end

if HUDManager then
    Hooks:PostHook(HUDManager, "_player_hud_layout", __Name("_player_hud_layout"), function(self)
        local name1 = __Name("name1")
        local panel1 = __Name("panel1")
        local bitmap1 = __Name("bitmap1")
        local hud = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
        self[panel1] = self[panel1] or hud and hud.panel or self._ws:panel({
            name = name1
        })
        self[bitmap1] = self[panel1]:bitmap({
            texture = ThisTexture,
            color = Color.white:with_alpha(1),
            layer = 1
        })
        self[bitmap1]:set_size(self[panel1]:w(), self[panel1]:h())
        self[bitmap1]:set_visible(false)
        _G[ThisBitmap] = self[bitmap1]
    end)
end
