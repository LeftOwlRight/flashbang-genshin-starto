local function YSQD_get_PeerUnit_by_PeerID(peer_id)
    local peer = managers.network:session():peer(peer_id)
    if peer then
        return peer:unit()
    end
end

local YSQD_modpath = ModPath
local volume_play_out = FlashBMemes and FlashBMemes.Options and FlashBMemes.Options:GetValue("__volume_start_others") or 0.5

Hooks:Add("NetworkReceivedData", "NetworkReceivedData_YSQD_Flashbang", function(sender, id, data)
	local peer_unit = YSQD_get_PeerUnit_by_PeerID(sender)
    local table_get_from_data = {}
    if data then
        table_get_from_data = json.decode(data)
    end

	local volume_play = FlashBMemes and FlashBMemes.Options and FlashBMemes.Options:GetValue("__volume_start_others") or volume_play_out
	if id == "YSQD_play" then
		if table_get_from_data and table_get_from_data.value_1 == "YSQD_play" and table_get_from_data.key_1 then
            local ready_path = YSQD_modpath.."sounds/cantaloupe.ogg"
            local last_char = string.sub(table_get_from_data.key_1, -5)
            if last_char == "2.ogg" then
                ready_path = YSQD_modpath.."sounds/cantaloupe_2.ogg"
            end
			blt.xaudio.setup()
			XAudio.UnitSource:new(peer_unit, XAudio.Buffer:new(ready_path)):set_volume(volume_play)
		end
	end
end)